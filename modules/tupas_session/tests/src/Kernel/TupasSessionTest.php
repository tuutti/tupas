<?php

namespace Drupal\Tests\tupas_session\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\tupas_session\Event\SessionData;
use Drupal\tupas_session\TupasSessionManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Test basic tupas session functionality.
 *
 * @group tupas
 */
class TupasSessionTest extends KernelTestBase {

  /**
   * The session manager.
   *
   * @var \Drupal\tupas_session\TupasSessionManager
   */
  protected $sessionManager;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'tupas',
    'tupas_session',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');
    $this->installEntitySchema('tupas_bank');
    $this->installConfig('tupas');
    $this->installSchema('tupas_session', 'tupas_session');
    $this->installConfig('tupas_session');

    // TupasSessionStorage::getOwner() requires this.
    $this->requestStack = new RequestStack();
    $request = Request::createFromGlobals();
    $this->requestStack->push($request);
    $this->requestStack->getCurrentRequest()->setSession(new Session());
    $this->container->set('request_stack', $this->requestStack);

    $this->sessionManager = $this->container->get('tupas_session.session_manager');
  }

  /**
   * Test tupas session expire.
   */
  public function testExpirableSessionStart() {
    $timestamp = time();

    $time = $this->getMockBuilder(TimeInterface::class)
      ->getMock();
    $time
      ->expects($this->at(0))
      ->method('getRequestTime')
      ->willReturn($timestamp);
    // Make sure renew() extends session access.
    $time->expects($this->at(1))
      ->method('getRequestTime')
      ->willReturn($timestamp + 5);

    $sessionManager = new TupasSessionManager(
      $this->container->get('config.factory'),
      $this->container->get('tupas_session.storage'),
      $this->container->get('session_manager'),
      $this->container->get('event_dispatcher'),
      $time
    );
    // Test session creation.
    $result = $sessionManager->start(random_int(10000, 100000), $this->randomString());
    $this->assertTrue($result);

    // Make sure getSession() returns valid session.
    $session = $sessionManager->getSession();
    $this->assertTrue($session instanceof SessionData);

    // Make sure renew() extends session access.
    $sessionManager->renew();
    $new_session = $sessionManager->getSession();
    $this->assertTrue($new_session->getAccess() > $session->getAccess());
  }

  /**
   * Test tupas session data.
   */
  public function testSessionData() {
    $this->sessionManager->start(random_int(100, 1000), $this->randomString(), [
      'random_test_data' => 1,
      'test' => ['this is array' => 1],
    ]);
    $session = $this->sessionManager->getSession();

    $this->assertEquals($session->getData('random_test_data'), 1);
  }

  /**
   * Test session destroy.
   */
  public function testSessionDestroy() {
    $this->sessionManager->start(random_int(10000, 100000), $this->randomString());
    $this->sessionManager->destroy();
    $this->assertFalse($this->sessionManager->getSession());
  }

  /**
   * Test garbage collection.
   */
  public function testGarbageCollection() {
    $timestamp = time();

    $time = $this->getMockBuilder(TimeInterface::class)
      ->getMock();
    $time
      ->expects($this->at(0))
      ->method('getRequestTime')
      ->willReturn($timestamp);
    // Manipulate request time to update last access time 31 minutes into past.
    $time->expects($this->at(1))
      ->method('getRequestTime')
      ->willReturn($timestamp - (31 * 60));

    $sessionManager = new TupasSessionManager(
      $this->container->get('config.factory'),
      $this->container->get('tupas_session.storage'),
      $this->container->get('session_manager'),
      $this->container->get('event_dispatcher'),
      $time
    );
    // Make sure session length is 30 minutes.
    $this->config('tupas_session.settings')
      ->set('tupas_session_length', 30)
      ->save();
    $sessionManager->start(random_int(10000, 100000), $this->randomString());
    // Test that gc() does not remove non expired sessions.
    $expire = $timestamp - 1800;
    $sessionManager->gc($expire);
    $this->assertTrue($sessionManager->getSession() instanceof SessionData);

    // Test that expired sessions gets removed.
    $sessionManager->renew();
    $sessionManager->gc($expire);
    $this->assertFalse($sessionManager->getSession());
  }

}
